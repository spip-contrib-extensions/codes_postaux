<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// C
	'codes_postaux_description' => 'Ajouter à la base de données les codes postaux récuperés sur le site geonames',
	'codes_postaux_nom' => 'Codes postaux',
	'codes_postaux_slogan' => 'Les codes postaux dans votre SPIP'
);
?>