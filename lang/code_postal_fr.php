<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(


	// A
	'ajouter_lien_code_postal' => 'Ajouter ce code postal',

	// E
	'explication_code' => '',

	// I
	'icone_creer_code_postal' => 'Créer un code postal',
	'icone_modifier_code_postal' => 'Modifier ce code postal',
	'info_1_code_postal' => 'Un code postal',
	'info_aucun_code_postal' => 'Aucun code postal',
	'info_codes_postaux_auteur' => 'Les codes postaux de cet auteur',
	'info_nb_codes_postaux' => '@nb@ de code postaux',

	// L
	'label_code' => 'Code',
	'label_titre' => 'Titre',

	// R
	'retirer_lien_code_postal' => 'Retirer ce code postal',
	'retirer_tous_liens_codes_postaux' => 'Retirer tous les codes postaux',

	// T
	'texte_ajouter_code_postal' => 'Ajouter un code postal',
	'texte_changer_statut_code_postal' => 'Ce code postal est :',
	'texte_creer_associer_code_postal' => 'Créer et associer un code postal',
	'titre_code_postal' => 'Code postal',
	'titre_codes_postaux' => 'Code postal',
	'titre_codes_postaux_rubrique' => 'Code postal de la rubrique',
	'titre_langue_code_postal' => 'Langue de ce code postal',
	'titre_logo_code_postal' => 'Logo de ce code postal',
);

?>
